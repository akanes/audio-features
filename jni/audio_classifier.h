#ifndef __AUDIO_CLASSIFIER_H__
#define __AUDIO_CLASSIFIER_H__

#include "jni_types.h"

void audioFeatureExtractionInit();

void audioFeatureExtractionDestroy();

//**********************************************************************************
//
// 	compute three features for voicing detection. Also a variable length autocorrelation values and
//	lags are stored in the returned double array
//
//**********************************************************************************
void extractFeatures(jshort* buff, jfloat * fVector,
		jfloat * obsProbVector, jbyte * inferRes, jint * numOfPeaks,
        jfloat * autoCorPeakVal, jshort* autoCorPeakLg);

#endif
