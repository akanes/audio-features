#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "audio_classifier.h"

void convert_short(char* buff, int len, int swap)
{
    int i;

    if(swap == 0)
    {
        return;
    }

    for (i = 0; i < len / 2; i++)
    {
        int si_0 = i * 2;
        int si_1 = si_0 + 1;

        char tmp = buff[si_0];
        buff[si_0] = buff[si_1];
        buff[si_1] = tmp;
    }
}

#ifdef TEST_MAIN

int main(int argc, char** argv)
{
    int fd = open("../data/test_interview_8k.raw", O_RDONLY);
    unsigned long frame_len = 256;
    unsigned long frame_shift = frame_len / 2;

    int swap = 0;

    jfloat fVector[6];
    jfloat obsProbVector[2];
    jbyte inferRes[20];
    jint numOfPeaks[1];
    jfloat autoCorPeakVal[128];
    jshort autoCorPeakLg[128];

    char* buff = (char*)malloc(frame_len * sizeof(short));

    int read_len = read(fd, buff, frame_len * sizeof(short));

    audioFeatureExtractionInit();

    if (read_len > 0)
    {
        convert_short(buff, frame_len * 2, swap);

        extractFeatures((jshort*)buff, fVector, obsProbVector, inferRes,
            numOfPeaks, autoCorPeakVal, autoCorPeakLg);
        memcpy(buff, buff + frame_shift * sizeof(short),
            frame_shift * sizeof(short));

        printf("%d\n", (int)inferRes[0]);
    }

    while (read(fd, buff+frame_shift * sizeof(short), frame_shift * sizeof(short)) > 0)
    {
        convert_short(buff+frame_shift * sizeof(short), frame_len * sizeof(short) / 2, swap);

        extractFeatures((jshort*)buff, fVector, obsProbVector, inferRes,
            numOfPeaks, autoCorPeakVal, autoCorPeakLg);
        memcpy(buff, buff + frame_shift * sizeof(short),
            frame_shift * sizeof(short));

        printf("%d\n", (int)inferRes[0]);
    }

    audioFeatureExtractionDestroy();

    return 0;
}

#endif

